package patterns.structural;

/*
Das Adapter Pattern wird verwendet, um die Schnittstellen von Klassen zu ändern, ohne den bestehenden Code zu ändern.
Hier ist eine Übung für das Adapter Pattern, bei der wir eine Anwendung entwickeln, um verschiedene Arten von Audio-Dateien abzuspielen.

Übung: Audio-Dateien mit dem Adapter Pattern

In dieser Übung erstellen wir eine Audio-Player-Anwendung, die verschiedene Arten von Audio-Dateien (z.B. MP3, WAV, FLAC) abspielen kann.
Wir werden das Adapter Pattern verwenden, um die verschiedenen Audio-Formate zu unterstützen.

Schritte:

1. Audio-Interfaces: Erstelle ein Audio-Interface (z.B. AudioPlayer), das die grundlegenden Methoden zum Abspielen von Audio-Dateien definiert, z.B. play(), pause(), stop().

2. Konkrete Audio-Klassen: Implementiere konkrete Audio-Klassen für verschiedene Audio-Formate, z.B. MP3Player, WAVPlayer, FLACPlayer.
Jede dieser Klassen sollte das AudioPlayer-Interface implementieren und die spezifischen Aktionen für das jeweilige Format durchführen.

3. Audio-Adapter-Interface: Erstelle ein Adapter-Interface (z.B. AudioAdapter), das auch das AudioPlayer-Interface implementiert.
Dieses Interface sollte eine Methode load(String file) enthalten, um die Audio-Datei zu laden und in ein unterstütztes Format zu konvertieren.

4. Konkrete Audio-Adapter-Klassen: Implementiere konkrete Adapter-Klassen, z.B. MP3Adapter, WAVAdapter, FLACAdapter.
Jede dieser Adapter-Klassen sollte das AudioAdapter-Interface implementieren und die load(String file)-Methode verwenden,
um die Audio-Datei in das unterstützte Format zu konvertieren.

5. Audio-Player-Anwendung: Schreibe ein Hauptprogramm, in dem du den Audio-Player verwendest, um verschiedene Audio-Dateien abzuspielen.
Du kannst sowohl die konkreten Audio-Klassen als auch die Adapter-Klassen verwenden, um verschiedene Formate abzuspielen.

Zusätzliche Herausforderung: Erweiterung der unterstützten Formate

Erweitere die Übung, indem du weitere Audio-Formate und Adapter-Klassen hinzufügst, um eine breitere Palette von Formaten zu unterstützen.

Diese Übung veranschaulicht, wie das Adapter Pattern verwendet werden kann, um die Zusammenarbeit zwischen verschiedenen Klassen und Formaten zu ermöglichen,
insbesondere in Bezug auf das Abspielen von Audio-Dateien. Es ist eine nützliche Übung, um das Adapter Pattern zu verstehen und anzuwenden.
 */

public class Adapter {

    interface AudioPlayer {
        void play();
        void pause();
        void stop();
    }

    static class MP3Player implements AudioPlayer {
        @Override
        public void play() {
            System.out.println("Spiel MP3-Datei ab.");
        }

        @Override
        public void pause() {
            System.out.println("MP3-Wiedergabe pausiert.");
        }

        @Override
        public void stop() {
            System.out.println("MP3-Wiedergabe gestoppt.");
        }
    }

    static class WAVPlayer implements AudioPlayer {
        @Override
        public void play() {
            System.out.println("Spielt WAV-Datei ab.");
        }

        @Override
        public void pause() {
            System.out.println("WAV-Wiedergabe pausiert.");
        }

        @Override
        public void stop() {
            System.out.println("WAV-Wiedergabe gestoppt.");
        }
    }

    static class FLACPlayer implements AudioPlayer {
        @Override
        public void play() {
            System.out.println("Spielt FLAC-Datei ab.");
        }

        @Override
        public void pause() {
            System.out.println("FLAC-Wiedergabe pausiert.");
        }

        @Override
        public void stop() {
            System.out.println("FLAC-Wiedergabe gestoppt.");
        }
    }

    interface AudioAdapter extends AudioPlayer {
        void load(String file);
    }

    static class MP3Adapter implements AudioAdapter {
        private MP3Player mp3Player;

        public MP3Adapter() {
            mp3Player = new MP3Player();
        }

        @Override
        public void load(String file) {
            System.out.println("Lädt und konvertiert MP3-Datei.");
        }

        @Override
        public void play() {
            mp3Player.play();
        }

        @Override
        public void pause() {
            mp3Player.pause();
        }

        @Override
        public void stop() {
            mp3Player.stop();
        }
    }

    static class WAVAdapter implements AudioAdapter {
        private WAVPlayer wavPlayer;

        public WAVAdapter() {
            wavPlayer = new WAVPlayer();
        }

        @Override
        public void load(String file) {
            System.out.println("Lädt und konvertiert WAV-Datei.");
        }

        @Override
        public void play() {
            wavPlayer.play();
        }

        @Override
        public void pause() {
            wavPlayer.pause();
        }

        @Override
        public void stop() {
            wavPlayer.stop();
        }
    }

    static class FLACAdapter implements AudioAdapter {
        private FLACPlayer flacPlayer;

        public FLACAdapter() {
            flacPlayer = new FLACPlayer();
        }

        @Override
        public void load(String file) {
            System.out.println("Lädt und konvertiert FLAC-Datei.");
        }

        @Override
        public void play() {
            flacPlayer.play();
        }

        @Override
        public void pause() {
            flacPlayer.pause();
        }

        @Override
        public void stop() {
            flacPlayer.stop();
        }
    }
}