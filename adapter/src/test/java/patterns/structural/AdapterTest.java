package patterns.structural;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AdapterTest {

  @Test
  public void adapterTest() {
    Adapter.AudioAdapter mp3Adapter = new Adapter.MP3Adapter();
    mp3Adapter.load("sample.mp3");
    mp3Adapter.play();

    Adapter.AudioAdapter wavAdapter = new Adapter.WAVAdapter();
    wavAdapter.load("sample.wav");
    wavAdapter.play();

    Adapter.AudioAdapter flacAdapter = new Adapter.FLACAdapter();
    flacAdapter.load("sample.flac");
    wavAdapter.play();
  }
}
