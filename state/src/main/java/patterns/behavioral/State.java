package patterns.behavioral;
/*
Übung: Zustandsautomat für ein Dokument

Stell dir vor, du entwickelst eine Anwendung, die ein Dokumentmodell verwaltet. Das Dokument kann verschiedene Zustände haben, z. B. "Neu", "In Bearbeitung" und "Abgeschlossen".
 Implementiere das State Pattern, um den Zustand des Dokuments zu verfolgen und das Verhalten entsprechend anzupassen.

Schritte:

1.Zustandsinterface: Erstelle ein Zustandsinterface (z.B. DocumentState), das die verschiedenen Zustände definiert. In diesem Fall könnten die Zustände "Neu", "In Bearbeitung" und "Abgeschlossen" sein.

2.Konkrete Zustandsklassen: Erstelle konkrete Zustandsklassen (z.B. NewDocumentState, EditingDocumentState, CompletedDocumentState), die das Zustandsinterface implementieren.
 Jede dieser Klassen enthält die Logik, die für den jeweiligen Zustand erforderlich ist.

3.Dokumentklasse: Erstelle eine Document-Klasse, die den aktuellen Zustand des Dokuments speichert und Methoden zum Ändern des Zustands sowie zur Ausführung von Aktionen basierend auf
 dem aktuellen Zustand bereitstellt. Diese Klasse sollte die Zustandsobjekte verwenden, um das Verhalten zu delegieren.

4.Testprogramm: Schreibe ein Hauptprogramm, das eine Document-Instanz erstellt und verschiedene Aktionen darauf ausführt, um den Übergang zwischen den Zuständen zu überprüfen. Überprüfe,
 ob das Dokument korrekt auf Aktionen in verschiedenen Zuständen reagiert.

Zusätzliche Herausforderung: Erweiterung des Zustandsmusters

Erweitere das Zustandsmuster, indem du weitere Zustände und Aktionen für das Dokument hinzufügst. Denke darüber nach, wie das Muster die Erweiterbarkeit und Wartbarkeit deiner Anwendung verbessert.

Diese Übung wird dir helfen, das State Pattern zu verstehen und zu üben, wie es genutzt werden kann, um das Verhalten eines Objekts basierend auf seinem internen Zustand zu steuern.
 */

public class State {
  interface DocumentState {
    void write(Document document, String content);

    void review(Document document);

    void complete(Document document);
  }

  static class NewDocumentState implements DocumentState {
    @Override
    public void write(Document document, String content) {
      document.setContent(content);
      document.setState(new EditingDocumentState());
    }

    @Override
    public void review(Document document) {
      System.out.println("Das Dokument kann nicht überprüft werden, es ist noch neu.");
    }

    @Override
    public void complete(Document document) {
      System.out.println(
          "Das Dokument kann nicht abgeschlossen werden, es muss zuerst bearbeitet werden.");
    }
  }

  static class EditingDocumentState implements DocumentState {
    @Override
    public void write(Document document, String content) {
      document.setContent(document.getContent() + "\n" + content);
    }

    @Override
    public void review(Document document) {
      document.setState(new ReviewDocumentState());
    }

    @Override
    public void complete(Document document) {
      System.out.println(
          "Das Dokument kann nicht abgeschlossen werden, es muss zuerst überprüft werden.");
    }
  }

  static class ReviewDocumentState implements DocumentState {
    @Override
    public void write(Document document, String content) {
      System.out.println("Das Dokument kann nicht bearbeitet werden, es wird überprüft.");
    }

    @Override
    public void review(Document document) {
      System.out.println("Das Dokument wird bereits überprüft.");
    }

    @Override
    public void complete(Document document) {
      document.setState(new CompletedDocumentState());
    }
  }

  static class CompletedDocumentState implements DocumentState {
    @Override
    public void write(Document document, String content) {
      System.out.println(
          "Das Dokument ist bereits abgeschlossen und kann nicht mehr überprüft werden.");
    }

    @Override
    public void review(Document document) {
      System.out.println(
          "Das Dokument ist bereits abgeschlossen und kann nicht mehr überprüft werden.");
    }

    @Override
    public void complete(Document document) {
      System.out.println("Das Dokument ist bereits abgeschlossen.");
    }
  }

  public static class Document {
    private DocumentState state;
    private String content;

    public Document() {
      state = new NewDocumentState();
      content = "";
    }

    public void setState(DocumentState state) {
      this.state = state;
    }

    public void setContent(String content) {
      this.content = content;
    }

    public String getContent() {
      return content;
    }

    public void write(String content) {
      state.write(this, content);
    }

    public void review() {
      state.review(this);
    }

    public void complete() {
      state.complete(this);
    }
  }
}
