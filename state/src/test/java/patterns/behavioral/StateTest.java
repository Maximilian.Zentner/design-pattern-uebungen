package patterns.behavioral;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StateTest {
  @Test
  public void DocumentStateTest() {
    System.out.println("\nState Pattern Ergebnisse:\n");
    State.Document document = new State.Document();

    document.write("Dies ist der Anfang des Dokuments.");
    document.review();
    document.write("Dies ist die Mitte des Dokuments.");
    document.complete();
    document.write("Dies ist das Ende des Dokuments.");

    document.review();
    document.complete();
    document.write("Versuch, ein abgeschlossenes Dokument zu bearbeiten.");
    System.out.println("\n");
  }
}
