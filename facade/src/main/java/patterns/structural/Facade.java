package patterns.structural;
/*
Das Fassadenmuster ist ein Entwurfsmuster, das eine vereinfachte Schnittstelle für eine Gruppe von Schnittstellen in einem Subsystem bereitstellt.
Hier ist ein Beispiel für eine Übung zum Fassadenmuster:

Übung: Buchungssystem-Fassade

Stell dir vor, du entwickelst ein Buchungssystem für eine Hotelkette. Das System hat mehrere komplexe Subsysteme wie Verfügbarkeitsprüfung,
Preisberechnung, Zimmerreservierung usw. Implementiere das Fassadenmuster, um eine einfache Schnittstelle für die Buchung von Hotelzimmern anzubieten.

Schritte:

1. Definiere die verschiedenen Klassen und Subsysteme, die für das Buchungssystem benötigt werden,
z. B. AvailabilityService, PricingService, ReservationService, usw.

2. Erstelle eine Fassadenklasse namens BookingFacade, die Methoden zur Buchung von Hotelzimmern bereitstellt.
Diese Klasse sollte die komplexen Interaktionen mit den Subsystemen abstrahieren.

3. Implementiere die Methoden in der BookingFacade, die die notwendigen Schritte zur Buchung eines Hotelzimmers orchestrieren,
z. B. Verfügbarkeit prüfen, Preis berechnen, Zimmer reservieren usw.

4. Erstelle ein Hauptprogramm, in dem du die BookingFacade verwendest, um Hotelzimmer zu buchen.
Dies sollte den Prozess vereinfachen, indem nur die Fassadenmethode aufgerufen wird, anstatt direkt mit den komplexen Subsystemen zu interagieren.

Zusätzliche Herausforderung: Erweiterung der Fassadenklasse

Erweitere die BookingFacade, um zusätzliche Funktionen hinzuzufügen, z. B. Stornierungen von Buchungen oder das Abrufen von Buchungsverlauf.

Diese Übung soll dir helfen, das Fassadenmuster zu verstehen und zu üben, wie man komplexe Subsysteme hinter einer einfachen Schnittstelle versteckt, um die Benutzung zu vereinfachen.
 */

public class Facade {

  static class AvailabilityService {
    public boolean checkAvailability(String roomType, int numberOfRooms) {
      //Verfügbarkeit
      return true;
    }
  }

  static class PricingService {
    public double calculatePrice(String roomType, int numberOfRooms, int nights) {
      // Preisberechnung
      return 100.0;
    }
  }

  static class ReservationService {
    public String reserveRoom(String roomType, int numberOfRooms, int nights) {
      // Zimmerreservierung
      return "123456";
    }
  }

  public static class BookingFacade {
    private AvailabilityService availabilityService;
    private PricingService pricingService;
    private ReservationService reservationService;

    public BookingFacade() {
      this.availabilityService = new AvailabilityService();
      this.pricingService = new PricingService();
      this.reservationService = new ReservationService();
    }

    public String bookRoom(String roomType, int numberOfRooms, int nights) {
      boolean isAvailable = availabilityService.checkAvailability(roomType, numberOfRooms);

      if (!isAvailable) {
        return "Room not available";
      }

      double totalPrice = pricingService.calculatePrice(roomType, numberOfRooms, nights);
      String reservationCode = reservationService.reserveRoom(roomType, numberOfRooms, nights);

      return "Booking successful. Reservation code: "
          + reservationCode
          + ", Total price: "
          + totalPrice;
    }
  }
}
