package patterns.structural;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FacadeTest {
    @Test
    public void BookingTest() {
        System.out.println("\nFacade Pattern Test:");

        Facade.BookingFacade bookingFacade = new Facade.BookingFacade();

        String bookingResult = bookingFacade.bookRoom("Single", 2, 3);
        System.out.println(bookingResult + "\n");
    }
}
