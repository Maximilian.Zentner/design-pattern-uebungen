package behavioral.Patterns.Observer;

    class Main {
        public static void main(String[] args) {
            System.out.println("\nObserver Pattern Ergebnisse:\n");
            Observer.WeatherStationData weatherStationData = new Observer.WeatherStationData();

            weatherStationData.registerObserver(new Observer.Display());
            weatherStationData.registerObserver(new Observer.App());

            weatherStationData.setMeasurements(25.5f, 60.0f, 1013.2f);
            weatherStationData.setMeasurements(22.0f, 55.5f, 1009.8f);

            weatherStationData.removeObserver(new Observer.Display());

            weatherStationData.setMeasurements(28.3f, 58.2f, 1010.5f);
        }
    }
