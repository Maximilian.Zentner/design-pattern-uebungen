package behavioral.Patterns.Observer;

/* Übung: Wetterstation mit Beobachtermuster

Stell dir vor, du entwickelst eine einfache Wetterstation. Du möchtest, dass verschiedene Anzeigegeräte (z.B. ein Display, eine App und eine Alarmvorrichtung) automatisch aktualisiert werden,
 wenn sich die Wetterbedingungen ändern. Implementiere das Observer Pattern, um dies zu erreichen.

 Schritte:

 1. Wetterdatenklasse: Erstelle eine Klasse WeatherData, die die aktuellen Wetterdaten wie Temperatur, Luftfeuchtigkeit und Luftdruck speichert.

 2. Beobachter-Interface: Definiere ein Beobachter-Interface, z.B. Observer, mit einer Methode update(), die von den Anzeigegeräten implementiert wird, um auf Änderungen der Wetterdaten zu reagieren.

 3. Konkrete Beobachter: Erstelle konkrete Beobachterklassen (z.B. Display, App, Alarm), die das Observer-Interface implementieren und die update()-Methode entsprechend umsetzen.
    Diese Klassen zeigen oder benachrichtigen über die Wetterdatenänderungen.

 4. Subjekt (Observable): Erstelle eine Klasse WeatherStation (oder WeatherStationData), die die Wetterdaten speichert und eine Liste von registrierten Beobachtern verwaltet. Implementiere Methoden,
    um Beobachter zu registrieren, zu entfernen und die Beobachter über Änderungen in den Wetterdaten zu benachrichtigen.

 5. Testprogramm: Schreibe ein Hauptprogramm, das eine Instanz der WeatherStation erstellt und einige Anzeigegeräte als Beobachter registriert.
  Simuliere dann Änderungen in den Wetterdaten und beobachte, wie die registrierten Anzeigegeräte automatisch aktualisiert werden.

 Zusätzliche Herausforderung: Erweiterung des Beobachtermusters

 Erweitere das Beobachtermuster, um verschiedene Arten von Beobachtern hinzuzufügen, die auf spezifische Wetterdaten (z.B. Temperatur, Luftfeuchtigkeit) reagieren.
  Dies erfordert eine präzisere Benachrichtigung der Beobachter über Änderungen in den Wetterdaten.

 Diese Übung hilft dir dabei, das Observer Pattern zu verstehen und zu üben, wie Objekte effektiv auf Änderungen in anderen Objekten reagieren können, ohne direkt miteinander gekoppelt zu sein.
  */

import java.util.ArrayList;
import java.util.List;

public class Observer {

    interface Observers {
        void update(float temperature, float humidity, float pressure);
    }

    public static class WeatherStationData {
        private List<Observers> observers = new ArrayList<>();
        private float temperature;
        private float humidity;
        private float pressure;

        public void registerObserver(Observers observers) {
            this.observers.add(observers);
        }

        public void removeObserver(Observers observers) {
            this.observers.remove(observers);
        }

        public void notifyObserver() {
            for (Observers observers : this.observers) {
                observers.update(temperature, humidity, pressure);
            }
        }

        public void setMeasurements(float temperature, float humidity, float pressure) {
            this.temperature = temperature;
            this.humidity = humidity;
            this.pressure = pressure;
            measurementsChanged();
        }

        private void measurementsChanged() {
            notifyObserver();
        }
    }

    public static class Display implements Observers {
        @Override
        public void update(float temperature, float humidity, float pressure) {
            System.out.println("Aktuelle Wetterdaten:");
            System.out.println("Temperatur: " + temperature + " °C");
            System.out.println("Luftfeuchtigkeit: " + humidity + " %");
            System.out.println("Luftdruck: " + pressure + " hPa");
            System.out.println();
        }
    }

    public static class App implements Observers {
        @Override
        public void update(float temperature, float humidity, float pressure) {
            System.out.println("Wetter-App aktualisiert.");
        }
    }
}