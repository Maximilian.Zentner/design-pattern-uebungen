package Structural_Patterns.Facade;

public class main {
    public static void main(String[] args){
        System.out.println("\nFacade Pattern Ergebnisse:\n");

        Facade.BookingFacade bookingFacade = new Facade.BookingFacade();

        String bookingResult = bookingFacade.bookRoom("Single", 2, 3);
        System.out.println(bookingResult + "\n");
    }
}
