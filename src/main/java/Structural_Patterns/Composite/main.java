package Structural_Patterns.Composite;

public class main {
  public static void main(String[] args) {
    System.out.println("\nComposite Pattern Ergebnisse:\n");
    Composite.Rectangle rectangle = new Composite.Rectangle(10, 20);
    Composite.Circle circle = new Composite.Circle(30, 40);

    Composite.CompositeGraphic group = new Composite.CompositeGraphic();
    group.add(rectangle);
    group.add(circle);

    System.out.println("Zeichne einzelne Elemente:");
    rectangle.draw();
    circle.draw();

    System.out.println("\nZeichne die Gruppe:");
    group.draw();

    System.out.println("\nBewege die Gruppe");
    group.move(5, 5);
    group.draw();
  }
}
