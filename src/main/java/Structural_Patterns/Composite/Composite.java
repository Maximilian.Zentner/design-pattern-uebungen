package Structural_Patterns.Composite;

/*
Übung: Grafische Elemente im Kompositmuster

Angenommen, du entwickelst eine Anwendung zur Erstellung von Grafiken, in der du einfache Formen (wie Rechtecke und Kreise) und Gruppen von Formen erstellen und bearbeiten möchtest.
 Implementiere das Composite Pattern, um diese hierarchische Struktur von grafischen Elementen zu modellieren.

Schritte:

1. Grafisches Element Interface: Erstelle ein Interface Graphic (grafisches Element), das Methoden für die grundlegenden Operationen enthält,
 die alle grafischen Elemente gemeinsam haben, z.B. das Zeichnen und das Ändern der Position.

2. Konkrete grafische Elemente: Implementiere konkrete Klassen, die das Graphic-Interface implementieren, z.B. Rectangle, Circle, usw.
    Jede dieser Klassen sollte die spezifischen Eigenschaften und Operationen der jeweiligen Form enthalten.

3. Komposite-Klasse: Erstelle eine Komposite-Klasse, z.B. CompositeGraphic, die das Graphic-Interface implementiert. Diese Klasse wird verwendet,
 um Gruppen von grafischen Elementen zu erstellen. Sie sollte eine Liste von Graphic-Objekten verwalten und die Operationen an diese Objekte weiterleiten.

4. Testprogramm: Schreibe ein Hauptprogramm, in dem du verschiedene grafische Elemente und Gruppen von Elementen erstellst und diese miteinander kombinierst.
 Zeige, wie das Composite Pattern verwendet wird, um sowohl einzelne Elemente als auch Gruppen von Elementen zu behandeln.

Zusätzliche Herausforderung: Grafische Transformationen

Erweitere die Übung, um grafische Transformationen wie Verschieben, Skalieren und Rotieren zu unterstützen. Dies erfordert, dass die Operationen auf allen Elementen in einer Gruppe rekursiv angewendet werden.

Diese Übung wird dir dabei helfen, das Composite Pattern zu verstehen und wie es verwendet werden kann, um eine hierarchische Struktur von Objekten zu erstellen und zu manipulieren.
 */

import java.util.ArrayList;
import java.util.List;

public class Composite {

    interface Graphic {
        void draw();

        void move(int x, int y);
    }

    public static class Rectangle implements Graphic {
        private int x, y;

        public Rectangle(int x, int y) {

            this.x = x;
            this.y = y;
        }

        @Override
        public void draw() {
            System.out.println("Zeichne ein Rechteck an Postion (" + x + ", " + y + ")");
        }

        @Override
        public void move(int x, int y) {
            this.x += x;
            this.y += y;
        }
    }

    public static class Circle implements Graphic {
        private int x, y;

        public Circle(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void draw() {
            System.out.println("Zeichne ein Kreis an Position (" + x + ", " + y + ")");
        }

        @Override
        public void move(int x, int y) {
            this.x += x;
            this.y += y;
        }
    }

    public static class CompositeGraphic implements Graphic {
        private List<Graphic> graphics = new ArrayList<>();

        @Override
        public void draw() {
            for (Graphic graphic : graphics) {
                graphic.draw();
            }
        }

        @Override
        public void move(int x, int y) {
            for (Graphic graphic : graphics) {
                graphic.move(x, y);
            }
        }

        public void add(Graphic graphic) {
            graphics.add(graphic);
        }

        public void remove(Graphic graphic) {
            graphics.remove(graphic);
        }
    }
}
