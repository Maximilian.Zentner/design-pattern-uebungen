package Creational_Patterns.Factory_Method;

public class main {
    public static void main(String[] args){
        Factory_Method.DocumentFactory textFactory = new Factory_Method.TextDocumentFactory();
        Factory_Method.Document textDocument = textFactory.createDocument();
        textDocument.save();
        textDocument.print();

        Factory_Method.DocumentFactory tableFactory = new Factory_Method.TabeleDoumentFactory();
        Factory_Method.Document tabeleDocument = tableFactory.createDocument();
        tabeleDocument.save();
        tabeleDocument.print();
    }
}
