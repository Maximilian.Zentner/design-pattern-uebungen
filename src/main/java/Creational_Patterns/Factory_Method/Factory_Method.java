package Creational_Patterns.Factory_Method;
/*
Das Factory Method Pattern wird verwendet, um die Erstellung von Objekten zu kapseln und in Subklassen zu delegieren. Hier ist eine Übung:

Übung: Erstellung von Dokumenten mit dem Factory Method Pattern

Angenommen, du entwickelst eine Textverarbeitungsanwendung, die verschiedene Arten von Dokumenten unterstützt, wie z.B. Textdokumente und Tabellendokumente.
Implementiere das Factory Method Pattern, um die Erstellung dieser Dokumente zu organisieren.

Schritte:

 1. Dokument-Abstraktion: Erstelle eine abstrakte Klasse Document, die die gemeinsamen Eigenschaften und Methoden für alle Dokumente definiert.
 Dies könnte beispielsweise eine Methode zum Speichern des Dokuments oder zum Drucken des Dokuments sein.

 2. Konkrete Dokumentklassen: Erstelle konkrete Dokumentklassen, die von der abstrakten Document-Klasse erben. Zum Beispiel TextDocument und TableDocument.
 Implementiere in diesen Klassen die spezifischen Methoden und Eigenschaften für jedes Dokumententyp.

 3. Factory Interface: Erstelle ein Factory-Interface, z.B. DocumentFactory, das eine abstrakte Methode createDocument() definiert, die die Erstellung von Dokumenten kapselt.

 4. Konkrete Fabrikklassen: Erstelle konkrete Fabrikklassen, die das DocumentFactory-Interface implementieren. Zum Beispiel TextDocumentFactory und TableDocumentFactory.
 In diesen Klassen implementierst du die createDocument()-Methode, um Instanzen der entsprechenden Dokumente zu erstellen.

 5. Testprogramm: Schreibe ein Hauptprogramm, in dem du verschiedene Dokumente erstellst, ohne explizit die konkreten Klassen zu instanziieren.
 Verwende stattdessen die Fabriken, um die Dokumente zu erstellen und dann gemeinsame Methoden der Document-Klasse aufzurufen.

Zusätzliche Herausforderung: Erweiterung der Dokumententypen

Erweitere die Übung, indem du weitere Dokumententypen und zugehörige Fabriken hinzufügst. Denke darüber nach, wie das Factory Method Pattern die Erstellung
neuer Dokumententypen erleichtert und die Anwendung skalierbarer macht.

Diese Übung wird dir helfen, das Factory Method Pattern zu verstehen und zu üben, wie es die Erstellung von Objekten in einem flexiblen und erweiterbaren System ermöglicht.
 */
public class Factory_Method {

  abstract static class Document {
    abstract void save();

    abstract void print();
  }

  static class TextDocument extends Document {
    @Override
    void save() {
      System.out.println("Textdokument wurde gespeichert.");
    }

    @Override
    void print() {
      System.out.println("Textdokument wurde gedruckt.");
    }
  }

  static class TableDocument extends Document {
    @Override
    void save() {
      System.out.println("Tabellendokument wurde gespeichert.");
    }

    @Override
    void print() {
      System.out.println("Tabellendokument wurde gedruckt.");
    }
  }

  interface DocumentFactory {
    Document createDocument();
  }

  static class TextDocumentFactory implements DocumentFactory {
    @Override
    public Document createDocument() {
      return new TextDocument();
    }
  }

  static class TabeleDoumentFactory implements DocumentFactory {
    @Override
    public Document createDocument() {
      return new TableDocument();
    }
  }
}
