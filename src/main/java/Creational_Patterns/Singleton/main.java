package Creational_Patterns.Singleton;

public class main {
    public static void main(String[] args){
        Singleton.Logger logger = Singleton.Logger.getInstance();

        logger.logInfo("Anwendung gestartet.");
        logger.logWarning("Niedriger Speicherplatz.");
        logger.logError("Fehler beim Speichern.");

        // Überprüfen, ob es sich immer um dieselbe Instanz handelt
        Singleton.Logger anotherLogger = Singleton.Logger.getInstance();
        if (logger == anotherLogger) {
            System.out.println("Die beiden Logger-Instanzen sind identisch.");
        }
    }
}
