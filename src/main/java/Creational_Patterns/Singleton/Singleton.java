package Creational_Patterns.Singleton;
/*
Das Singleton Pattern wird verwendet, um sicherzustellen, dass eine Klasse nur eine einzige Instanz hat und den Zugriff darauf zentralisiert.
In dieser Übung erstellen wir einen einfachen Logger, der das Singleton Pattern verwendet.

Übung: Singleton Logger

Stelle dir vor, du entwickelst eine Anwendung, die verschiedene Teile enthält, und du möchtest Informationen zu den Aktivitäten dieser Teile protokollieren.
Implementiere das Singleton Pattern, um einen Logger zu erstellen, der die Protokollierung der Aktivitäten in deiner Anwendung verwaltet.

Schritte:

 1. Singleton Logger: Erstelle eine Logger-Klasse, die das Singleton Pattern verwendet. Diese Klasse sollte eine Methode zum Protokollieren von Nachrichten (z.B. log(String message))
 und eine Methode zum Abrufen des Logger-Objekts (getInstance()) bereitstellen.

 2. Protokollierungsfunktionen: Implementiere in der Logger-Klasse Methoden zum Protokollieren von Nachrichten, z.B. logInfo(String message),
 logWarning(String message), und logError(String message). Diese Methoden sollten die Nachrichten mit entsprechenden Kennzeichnungen (z.B. "[INFO]", "[WARNING]", "[ERROR]") protokollieren.

 3. Testprogramm: Schreibe ein Hauptprogramm, in dem du den Logger verwendest, um Nachrichten unterschiedlicher Art zu protokollieren.
 Stelle sicher, dass du den Logger nur einmal initialisierst und dann über die getInstance()-Methode darauf zugreifst.

Zusätzliche Herausforderung: Logger-Konfiguration

Erweitere die Übung, indem du eine Logger-Konfiguration hinzufügst. Dies könnte bedeuten, dass du den Protokollierungspegel (z.B. "Debug", "Info", "Warning")
und die Ausgaberichtung (z.B. Konsole, Datei) konfigurieren kannst.

Diese Übung wird dir dabei helfen, das Singleton Pattern zu verstehen und zu üben, wie es verwendet wird, um sicherzustellen,
dass nur eine Instanz einer Klasse existiert und diese zentralisiert aufgerufen werden kann. Außerdem wirst du den Umgang mit Protokollierungsanforderungen in deiner Anwendung üben.
 */

public class Singleton {

    class Logger {
        private static Logger instance;

        private Logger() {}

        public static Logger getInstance() {
            if (instance == null) {
                instance = new Logger();
            }
            return instance;
        }
    }

    public void log(String message) {
        System.out.println(message);
    }

    public void logInfo(String message) {
        log("[INFO]" + message);
    }

    public void logWarning(String message) {
        logWarning("[WARNING]" + message);
    }

    public void logError(String message) {
        logError("[ERROR]" + message);
    }
}
