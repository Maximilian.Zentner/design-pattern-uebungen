package Creational_Patterns.Builder;
/*
Das Builder Design Pattern ist ein Entwurfsmuster, das verwendet wird, um die Erstellung komplexer Objekte zu vereinfachen,
 indem es den Konstruktionsprozess von der eigentlichen Objekterstellung trennt. Hier ist ein Beispiel für Übungen zum Builder Design Pattern:

Übung 1: Einführung in das Builder Design Pattern

Gegeben ist die Aufgabe, ein Auto-Objekt zu erstellen, das verschiedene Eigenschaften wie Marke, Modell,
 Farbe und Optionen haben soll. Implementiere das Builder Design Pattern, um die Erstellung eines Auto-Objekts zu vereinfachen.

Schritte:
    1.Erstelle eine Klasse Car mit den Attributen für Marke, Modell, Farbe und Optionen.
    2.Definiere ein Interface oder eine abstrakte Klasse CarBuilder, die Methoden zum Setzen der Eigenschaften des Autos enthält.
    3.Erstelle konkrete Klassen, die von CarBuilder erben, z. B. StandardCarBuilder und LuxuryCarBuilder. Diese Klassen implementieren die Methoden zum Setzen der Eigenschaften des Autos.
    4.Erstelle eine Director-Klasse CarDirector, die eine Methode zum Erstellen eines Auto-Objekts mit Hilfe eines CarBuilders enthält.
    5.Implementiere ein Hauptprogramm, in dem du das Builder Design Pattern verwendest, um verschiedene Arten von Autos zu erstellen.

Übung 2: Erweiterung des Builder Patterns

Baue auf der vorherigen Übung auf, um das Builder Design Pattern für die Erstellung von komplexen Objekten zu erweitern.

Schritte:
    1.Ergänze das CarBuilder Interface oder die abstrakte Klasse um weitere Methoden, die zur Einstellung von spezifischen Optionen dienen, z. B. setSunroof(), setLeatherSeats(), usw.
    2.Passe die konkreten Builder-Klassen an, um die neuen Methoden zu implementieren.
    3.Passe den Director an, um die neuen Methoden für die Konfiguration der Autos zu verwenden.
    4.Erstelle verschiedene Autos mit unterschiedlichen Konfigurationen, indem du die neuen Methoden der Builder und des Directors verwendest.

Übung 3: Anwendung des Fluent Interface mit dem Builder

Fluent Interface ermöglicht die Verkettung von Methodenaufrufen, um den Code lesbarer zu gestalten. Erweitere das Builder Design Pattern, um ein Fluent Interface zu verwenden.

Schritte:
    1.Passe das CarBuilder Interface oder die abstrakte Klasse an, um jede Methode setXXX die return this zurückgibt, um Methodenaufrufe zu verkettet.
    2.Passe die konkreten Builder-Klassen entsprechend an.
    3.Teste die Verwendung des Fluent Interface durch die Erstellung von Auto-Objekten mit verketteten Methodenaufrufen.

Diese Übungen sollten dir helfen, das Builder Design Pattern besser zu verstehen und seine Anwendung in verschiedenen Kontexten zu üben.
 */
public class Builder {
    // Produktklasse
    static class Car {
        private String brand;
        private String model;
        private String color;
        private boolean sunroof;
        private boolean leatherSeats;

        // Konstruktor und Getter-Methoden...

        @Override
        public String toString() {
            return "Car{" +
                    "brand='" + brand + '\'' +
                    ", model='" + model + '\'' +
                    ", color='" + color + '\'' +
                    ", sunroof=" + sunroof +
                    ", leatherSeats=" + leatherSeats +
                    '}';
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public void setSunroof(boolean sunroof) {
            this.sunroof = sunroof;
        }

        public void setLeatherSeats(boolean leatherSeats) {
            this.leatherSeats = leatherSeats;
        }
    }

    // Builder-Interface
    interface CarBuilder {
        CarBuilder setBrand(String brand);
        CarBuilder setModel(String model);
        CarBuilder setColor(String color);
        CarBuilder setSunroof(boolean sunroof);
        CarBuilder setLeatherSeats(boolean leatherSeats);
        Car build();
    }

    // Konkreter Builder für Standardautos
    static class StandardCarBuilder implements CarBuilder {
        private Car car = new Car();

        @Override
        public CarBuilder setBrand(String brand) {
            car.setBrand(brand);
            return this;
        }

        @Override
        public CarBuilder setModel(String model) {
            car.setModel(model);
            return this;
        }

        @Override
        public CarBuilder setColor(String color) {
            car.setColor(color);
            return this;
        }

        @Override
        public CarBuilder setSunroof(boolean sunroof) {
            car.setSunroof(sunroof);
            return this;
        }

        @Override
        public CarBuilder setLeatherSeats(boolean leatherSeats) {
            car.setLeatherSeats(leatherSeats);
            return this;
        }

        @Override
        public Car build() {
            return car;
        }
    }

    // Director-Klasse
    static class CarDirector {
        private CarBuilder builder;

        public CarDirector(CarBuilder builder) {
            this.builder = builder;
        }

        public Car buildLuxuryCar() {
            return builder.setBrand("Mercedes")
                    .setModel("S-Class")
                    .setColor("Black")
                    .setSunroof(true)
                    .setLeatherSeats(true)
                    .build();
        }

        public Car buildStandardCar() {
            return builder.setBrand("Toyota")
                    .setModel("Camry")
                    .setColor("Silver")
                    .build();
        }
    }
}
