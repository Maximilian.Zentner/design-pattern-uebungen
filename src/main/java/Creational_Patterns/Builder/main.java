package Creational_Patterns.Builder;

public class main {
        public static void main(String[] args) {
            System.out.println("\nBuilder Pattern Ergebnisse:\n");
            Builder.CarBuilder builder = new Builder.StandardCarBuilder();
            Builder.CarDirector director = new Builder.CarDirector(builder);

            Builder.Car standardCar = director.buildStandardCar();
            System.out.println("Standard Car: " + standardCar);

            Builder.Car luxuryCar = director.buildLuxuryCar();
            System.out.println("Luxury Car: " + luxuryCar);
        }

}
