package patterns.command;

// Das Command Pattern wird verwendet, um Anfragen als objektorientierte Objekte zu kapseln,
// wodurch sie parametrisierbar und rückgängig machbar werden. In dieser Übung werden wir ein
// einfaches Beispiel mit einem Texteditor erstellen.
//
// Übung: Command Pattern in einem Texteditor
//
// Stellen wir uns vor, du entwickelst einen Texteditor und möchtest eine Undo/Redo-Funktionalität
// implementieren. Verwende das Command Pattern, um diese Funktionalität zu realisieren.
//
// Schritte:
//
// 1. Kommando-Interface: Erstelle ein Kommando-Interface (z.B. Command), das eine Methode execute()
// enthält, um die Aktion auszuführen.
//
// 2. Konkrete Kommando-Klassen: Implementiere konkrete Kommando-Klassen, die von dem
// Kommando-Interface erben. Beispiele könnten InsertCommand und DeleteCommand sein, die die
// Aktionen zum Einfügen und Löschen von Text darstellen.
//
// 3. Texteditor-Klasse: Erstelle eine Texteditor-Klasse (z.B. TextEditor), die Methoden zum Ausführen
// der Aktionen, zum Rückgängigmachen und Wiederherstellen der Aktionen hat.
//
// 4. Kommando-Historie: Implementiere eine Kommando-Historie-Klasse (z.B. CommandHistory), die die
// ausgeführten Kommandos speichert, um die Undo/Redo-Funktionalität zu ermöglichen.
//
// 5. Testprogramm: Schreibe ein Hauptprogramm, in dem du den Texteditor verwendest, um Text
// hinzuzufügen, zu löschen und diese Aktionen rückgängig zu machen bzw. wiederherzustellen.
//
// Zusätzliche Herausforderung: Parameterisierung von Kommandos
//
// Erweitere die Übung, indem du den Kommandos Parameter hinzufügst. Zum Beispiel könntest du das
// InsertCommand-Kommando so erweitern, dass es Text erhält, der eingefügt werden soll.
//
// Diese Übung hilft dir zu verstehen, wie das Command Pattern verwendet wird, um Aktionen in
// objektorientierten Objekten zu kapseln und die Möglichkeit zum Rückgängigmachen und
// Wiederherstellen von Aktionen bereitzustellen. Es ist auch ein gutes Beispiel, wie das Muster in
// realen Anwendungen verwendet wird.

import java.util.ArrayList;
import java.util.List;

public class Command {

    interface Commander {
        void execute();
    }

    static class InsertCommand implements Commander {
        private TextEditor editor;
        private String textToInsert;

        public InsertCommand(TextEditor editor, String textToInsert) {
            this.editor = editor;
            this.textToInsert = textToInsert;
        }

        @Override
        public void execute() {
            editor.insert(textToInsert);
        }
    }

    static class DeleteCommand implements Commander {
        private TextEditor editor;
        private String deletedText;

        public DeleteCommand(TextEditor editor) {
            this.editor = editor;
        }

        @Override
        public void execute() {
            deletedText = editor.delete();
        }

        public String getDeletedText() {
            return deletedText;
        }
    }

    static class TextEditor {
        private StringBuilder text = new StringBuilder();

        public void insert(String textToInsert) {
            text.append(textToInsert);
        }

        public String delete() {
            int length = text.length();
            if (length > 0) {
                String deletedText = text.substring(length - 1);
                text.deleteCharAt(length - 1);
                return deletedText;
            }
            return "";
        }

        public String getText() {
            return text.toString();
        }
    }

    static class CommandHistory {
        private List<Commander> history = new ArrayList<>();
        private int currentIndex = -1;

        public void execute(Commander command) {
            command.execute();
            history.add(command);
            currentIndex++;
        }

        public void undo() {
            if (currentIndex >= 0) {
                Commander command = history.get(currentIndex);
                command.execute(); // Umkehrung des Kommandos
                currentIndex--;
            }
        }

        public void redo() {
            if (currentIndex < history.size() - 1) {
                currentIndex++;
                Commander command = history.get(currentIndex);
                command.execute();
            }
        }
    }
}
