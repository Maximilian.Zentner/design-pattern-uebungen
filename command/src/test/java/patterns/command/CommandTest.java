package patterns.command;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommandTest {
    @Test
    public void Command() {
        Command.TextEditor editor = new Command.TextEditor();
        Command.CommandHistory history = new Command.CommandHistory();

        Command.InsertCommand insertCommand1 = new Command.InsertCommand(editor, "Hallo, ");
        history.execute(insertCommand1);

        Command.InsertCommand insertCommand2 = new Command.InsertCommand(editor, "Welt!");
        history.execute(insertCommand2);

        System.out.println("Text nach Einfügen: " + editor.getText());

        Command.DeleteCommand deleteCommand = new Command.DeleteCommand(editor);
        history.execute(deleteCommand);

        System.out.println("Gelöschter Text: " + ((Command.DeleteCommand) deleteCommand).getDeletedText());
        System.out.println("Text nach Löschen: " + editor.getText());

        history.undo();
        System.out.println("Text nach Undo: " + editor.getText());

        history.redo();
        System.out.println("Text nach Redo: " + editor.getText());
}
}
