package patterns.creational;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PrototypeTest {

  @Test
  public void prototypeTest() {

    Prototype.BasicPlaylist originalPlaylist = new Prototype.BasicPlaylist();
    originalPlaylist.addSong("Song 1");
    originalPlaylist.addSong("Song 2");

    Prototype.PlaylistPrototype prototype = new Prototype.PlaylistCloneable(originalPlaylist);

    Prototype.Playlist clonedPlaylist = prototype.clone();
    clonedPlaylist.addSong("Song 3");

    System.out.println("Original Playlist:");
    originalPlaylist.display();

    System.out.println("\nGeklonte Playlist:");
    clonedPlaylist.display();
  }
}
