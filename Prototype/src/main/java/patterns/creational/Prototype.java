package patterns.creational;

/*
Das Prototype Pattern wird verwendet, um neue Objekte zu erstellen, indem sie Kopien eines bestehenden Objekts verwenden.
In dieser Übung erstellen wir einen einfachen Klon eines Musik-Playlistsystems.

        Übung: Musik-Playlist mit dem Prototype Pattern

        Stellen wir uns vor, du entwickelst eine Musik-Playlist-Anwendung, in der Benutzer Playlists erstellen können.
        Du möchtest das Prototype Pattern verwenden, um die Erstellung von Playlists zu unterstützen.

        Schritte:

        1. Playlist-Interface: Erstelle ein Playlist-Interface (z.B. Playlist) mit Methoden zum Hinzufügen von Songs und zum Anzeigen der Playlist.

        2. Konkrete Playlist-Klasse: Implementiere eine konkrete Klasse, die das Playlist-Interface implementiert, z.B. BasicPlaylist.
            Diese Klasse sollte eine Liste von Songs verwalten und die Methoden des Interfaces implementieren.

        3. Prototype-Interface: Erstelle ein Prototype-Interface (z.B. PlaylistPrototype), das eine Methode clone() enthält, um eine Kopie der Playlist zu erstellen.

        4. Konkrete Prototype-Klasse: Implementiere eine konkrete Klasse, die das PlaylistPrototype-Interface implementiert, z.B. PlaylistCloneable.
            Diese Klasse sollte eine Methode clone() bereitstellen, um eine Kopie der Playlist zu erstellen.

        5. Testprogramm: Schreibe ein Hauptprogramm, in dem du das Prototype Pattern verwendest, um Playlists zu klonen und Änderungen in einem Klon vorzunehmen,
            ohne das Original zu beeinflussen.

        Zusätzliche Herausforderung: Komplexe Klonoperationen

        Erweitere die Übung, indem du komplexe Klonoperationen durchführst.
        Das könnte das Klonen von Songs innerhalb der Playlist oder das Erstellen von Klonen von Songs aus einer gemeinsamen Sammlung von Songs sein.

        Diese Übung hilft dir, das Prototype Pattern zu verstehen und zu üben, wie es verwendet wird, um Kopien von Objekten zu erstellen,
        ohne die Originalobjekte zu beeinflussen. Es ist auch ein gutes Beispiel dafür, wie das Muster in realen Anwendungen verwendet wird.
*/

import java.util.ArrayList;
import java.util.List;

public class Prototype {

  interface Playlist {
    void addSong(String song);

    void display();
  }

  static class BasicPlaylist implements Playlist {
    private List<String> songs = new ArrayList<>();

    @Override
    public void addSong(String song) {
      songs.add(song);
    }

    @Override
    public void display() {
      System.out.println("Playlist:");
      for (String song : songs) {
        System.out.println(song);
      }
    }
  }

  interface PlaylistPrototype {
    Playlist clone();
  }

  static class PlaylistCloneable implements PlaylistPrototype {
    private BasicPlaylist playlist;

    public PlaylistCloneable() {
      playlist = new BasicPlaylist();
    }

    public PlaylistCloneable(BasicPlaylist playlist) {
      this.playlist = new BasicPlaylist();
      for (String song : playlist.songs) {
        this.playlist.addSong(song);
      }
    }

    @Override
    public Playlist clone() {
      BasicPlaylist clonedPlaylist = new BasicPlaylist();
      for (String song : this.playlist.songs) {
        clonedPlaylist.addSong(song);
      }
      return clonedPlaylist;
    }
  }
}
