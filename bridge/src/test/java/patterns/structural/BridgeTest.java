package patterns.structural;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BridgeTest {
    @Test
    public void bridgeTest() {
        Bridge.Platform windows = new Bridge.WindowsPlatform();
        Bridge.Platform linux = new Bridge.LinuxPlatform();

        Bridge.Shape lineOnWindows = new Bridge.Line(windows);
        Bridge.Shape circleOnLinux = new Bridge.Circle(linux);

        windows.initialize();
        lineOnWindows.draw();
        linux.initialize();
        circleOnLinux.draw();

        windows.dispose();
        linux.dispose();
    }
}