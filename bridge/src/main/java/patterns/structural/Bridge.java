package patterns.structural;

/*
Hier ist eine Übung zum Bridge Pattern, bei der wir ein einfaches Zeichnungsprogramm erstellen, das verschiedene Formen auf verschiedenen Plattformen zeichnen kann.

Übung: Zeichnungsprogramm mit dem Bridge Pattern

In dieser Übung entwickeln wir ein Zeichnungsprogramm, das auf verschiedenen Plattformen (z.B. Windows, Linux) verschiedene Formen (z.B. Linien, Kreise) zeichnen kann.
Wir werden das Bridge Pattern verwenden, um die Trennung zwischen Formen und Plattformen zu erreichen.

Schritte:

1. Abstrakte Formenklasse: Erstelle eine abstrakte Formenklasse (z.B. Shape), die Methoden zum Zeichnen von Formen definiert, z.B. draw().
   Diese Klasse wird die Brücke zwischen den Formen und den Plattformen sein.

2. Konkrete Formenklassen: Implementiere konkrete Formenklassen, die von der abstrakten Formenklasse erben, z.B. Line und Circle.
   Jede dieser Klassen sollte die draw()-Methode überschreiben, um die spezifischen Zeichenanweisungen für die Form zu enthalten.

3. Abstrakte Plattformklasse: Erstelle eine abstrakte Plattformklasse (z.B. Platform),
   die Methoden zur Initialisierung und Freigabe der Plattform definiert, z.B. initialize() und dispose().

4. Konkrete Plattformklassen: Implementiere konkrete Plattformklassen, die von der abstrakten Plattformklasse erben,
   z.B. WindowsPlatform und LinuxPlatform. Jede dieser Klassen sollte die Initialisierungs- und Freigabemethoden implementieren, um die spezifischen Plattformdetails zu behandeln.

5. Bridge zwischen Formen und Plattformen: Erstelle eine Brücke zwischen Formen und Plattformen,
   indem du die abstrakte Formenklasse und die abstrakte Plattformklasse in einem Konstruktor kombinierst, z.B. Shape(Platform platform).
   Die draw()-Methode in der Formenklasse sollte die Plattformmethoden aufrufen, um die Form zu zeichnen.

6. Zeichnungsprogramm-Anwendung: Schreibe ein Hauptprogramm, in dem du verschiedene Formen auf verschiedenen Plattformen zeichnest.
   Erstelle Formen mit einer spezifischen Plattform und rufe dann die draw()-Methode auf, um die Form auf dieser Plattform zu zeichnen.

    Zusätzliche Herausforderung: Weitere Formen und Plattformen

    Erweitere die Übung, indem du zusätzliche Formen und Plattformen hinzufügst, um die Flexibilität und den Nutzen des Bridge Patterns zu demonstrieren.

    Diese Übung zeigt, wie das Bridge Pattern verwendet wird, um die Zusammenarbeit zwischen abstrakten Entitäten (Formen) und
    Implementierungen (Plattformen) zu ermöglichen und die Flexibilität bietet, verschiedene Formen auf verschiedenen Plattformen zu zeichnen.
    Es ist ein gutes Beispiel für die Verwendung des Bridge Patterns in der Praxis.
 */
public class Bridge {

     static abstract class Shape {
        protected Platform platform;

        public Shape(Platform platform) {
            this.platform = platform;
        }

        public abstract void draw();
    }

    static class Line extends Shape {
        public Line(Platform platform) {
            super(platform);
        }

        @Override
        public void draw() {
            System.out.println("Zeichne eine Linie auf " + platform.getName());
        }
    }

    static class Circle extends Shape {
        public Circle(Platform platform) {
            super(platform);
        }

        @Override
        public void draw() {
            System.out.println("Zeichne einen Kreis auf " + platform.getName());
        }
    }

    static abstract class Platform {
        public abstract void initialize();
        public abstract void dispose();
        public abstract String getName();
    }

    static class WindowsPlatform extends Platform {
        @Override
        public void initialize() {
            System.out.println("Initialisiere Windows-Plattform");
        }

        @Override
        public void dispose() {
            System.out.println("Beende Windows-Plattform");
        }

        @Override
        public String getName() {
            return "Windows";
        }
    }

    static class LinuxPlatform extends Platform {
        @Override
        public void initialize() {
            System.out.println("Initialisiere Linux-Plattform");
        }

        @Override
        public void dispose() {
            System.out.println("Beende Linux-Plattform");
        }

        @Override
        public String getName() {
            return "Linux";
        }
    }
}
