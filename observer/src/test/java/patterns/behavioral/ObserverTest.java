package patterns.behavioral;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ObserverTest {
    @Test
    public void WeatherStationTest() {
       System.out.println("\nObserver Pattern WeatherStationTest");
       Observer.WeatherStationData weatherData = new Observer.WeatherStationData();

       weatherData.registerObserver(new Observer.Display());
       weatherData.registerObserver(new Observer.App());

       weatherData.setMeasurements(25.5f, 60.0f, 1013.2f);
       weatherData.setMeasurements(22.0f, 55.5f, 1009.8f);

       weatherData.removeObserver(new Observer.Display());
       weatherData.setMeasurements(28.3f,58.2f,1010.5f);
    }
}
