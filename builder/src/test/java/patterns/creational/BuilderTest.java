package patterns.creational;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BuilderTest {

    @Test
    public void testBuildLuxuryCar() {
        Builder.CarBuilder builder = new Builder.CarBuilder();
        Builder.Car car = builder
                .setBrand("Mercedes")
                .setModel("S-Class")
                .setColor("Black")
                .setSunroof(true)
                .setLeatherSeats(true)
                .build();

        assertEquals("Car{brand='Mercedes', model='S-Class', color='Black', sunroof=true, leatherSeats=true}", car.toString());
    }

    @Test
    public void testBuildStandardCar() {
        Builder.CarBuilder builder = new Builder.CarBuilder();
        Builder.Car car = builder
                .setBrand("Toyota")
                .setModel("Camry")
                .setColor("Silver")
                .build();

        assertEquals("Car{brand='Toyota', model='Camry', color='Silver', sunroof=false, leatherSeats=false}", car.toString());
    }
}