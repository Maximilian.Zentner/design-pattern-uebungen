package patterns.composite;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CompositeTest {

    @Test
 public void Compsite(){
    Composite.Rectangle rectangle = new Composite.Rectangle(10,20);
    Composite.Circle circle = new Composite.Circle(30,40);

    Composite.CompositeGraphic group = new Composite.CompositeGraphic();
    group.add(rectangle);
    group.add(circle);

    System.out.println("Zeichne einzelne Elemente");
    rectangle.draw();
    circle.draw();

    System.out.println("\nZeichne die Gruppe:");
    group.draw();

    System.out.println("\nBewegt die Gruppe");
    group.move(5, 5);
    group.draw();
}
}