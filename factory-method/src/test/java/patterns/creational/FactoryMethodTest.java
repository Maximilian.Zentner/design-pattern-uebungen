package patterns.creational;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FactoryMethodTest {

    @Test
    public void DocumentFactoryTest() {

    FactoryMethod.DocumentFactory textFactory = new FactoryMethod.TextDocumentFactory();
    FactoryMethod.Document textDocument = textFactory.createDocument();
    textDocument.save();
    textDocument.print();

    FactoryMethod.DocumentFactory tableFactory = new FactoryMethod.TabeleDoumentFactory();
    FactoryMethod.Document tabeleDocument = tableFactory.createDocument();
    tabeleDocument.save();
    tabeleDocument.print();
    }
}