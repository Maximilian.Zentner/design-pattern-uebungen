package patterns.creational;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SingletonTest {

  @Test
  public void SingletonLoggerTest() {

    Singleton.Logger logger = Singleton.Logger.getInstance();

    logger.logInfo("Anwendung gestartet.");
    logger.logWarning("Niedriger Speicherplatz.");
    logger.logError("Fehler beim Speichern.");

    Singleton.Logger anotherLogger = Singleton.Logger.getInstance();
    assertSame(logger, anotherLogger, "Die beiden Logger-Instanzen sind identisch.");
  }
}
